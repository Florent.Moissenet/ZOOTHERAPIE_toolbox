% Author     :   F. Moissenet
%                Kinesiology Laboratory (K-LAB)
%                University of Geneva
%                https://www.unige.ch/medecine/kinesiology
% License    :   Creative Commons Attribution-NonCommercial 4.0 International License 
%                https://creativecommons.org/licenses/by-nc/4.0/legalcode
% Source code:   https://gitlab.unige.ch/KLab/klab_zootherapie
% Reference  :   Not applicable
% Date       :   December 2019
% -------------------------------------------------------------------------
% Description:   This toolbox loads CSV files exported from Physilog RTK 
%                (GaitUp, Switzerland) and compute basic accelerometer-
%                based parameters for lower and upper limbs.
% Dependencies : GaitUp Physilog Matlab ToolKit (https://gaitup.com/support/)
% -------------------------------------------------------------------------
% This work is licensed under the Creative Commons Attribution - 
% NonCommercial 4.0 International License. To view a copy of this license, 
% visit http://creativecommons.org/licenses/by-nc/4.0/ or send a letter to 
% Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
% -------------------------------------------------------------------------

clearvars;
close all;
clc;

% -------------------------------------------------------------------------
% TOOLBOX FOLDERS
% -------------------------------------------------------------------------
Folder.toolbox = 'C:\Users\moissene\Documents\Professionnel\routines\github\KLAB_Zootherapie\';
Folder.data    = 'C:\Users\moissene\Documents\Professionnel\projets recherche\2020 - ZOOTHERAPIE\Donn�es tests\patient 1\';
Folder.dep     = 'C:\Users\moissene\Documents\Professionnel\routines\github\KLAB_Zootherapie\KLAB_Zootherapie\dependencies\';
template       = 'ZOOTHERAPIE_modele.xlsx';
addpath(Folder.toolbox);
addpath(genpath(Folder.dep)); % Adds the specified folders to the top of the search path for the current session

% -------------------------------------------------------------------------
% EXTRACT DATA
% -------------------------------------------------------------------------
cd(Folder.data);
binFiles = dir('*.BIN'); % all bin files must be in the subject folder and renamed with the measurement date, INF/SUP and file number (e.g. 19_12_19_INF_001.BIN)

for icondition = 1:size(binFiles,1)
    
    % ---------------------------------------------------------------------
    % CLEAR WORKSPACE
    % ---------------------------------------------------------------------
    clearvars -except Folder binFiles template icondition Outcomes
    
    % ---------------------------------------------------------------------
    % READ FILE
    % ---------------------------------------------------------------------
    [sensorData, header] = rawP5reader(binFiles(icondition).name,'sync');
    date                 = [header.startDate.Year,'-',header.startDate.Month,'-',header.startDate.Day]; % YYYY-MM-DD
    n                    = size(sensorData(1).data(:,1),1);
    f                    = sensorData(1).Fs;                                                            % Hz
    time                 = sensorData(1).timestamps;                                                    % s
    acc(:,1)             = sensorData(1).data(:,1)*9.81;                                                % m.s-2
    acc(:,2)             = sensorData(1).data(:,2)*9.81;                                                % m.s-2
    acc(:,3)             = sensorData(1).data(:,3)*9.81;                                                % m.s-2

    % ---------------------------------------------------------------------
    % PARAMETERS RELATED TO LOWER LIMB ACTIVITY
    % ---------------------------------------------------------------------
    if strfind(binFiles(icondition).name,'cuisse')
        
        % COMPUTE THE QUANTITY OF ACTIVITY
        % -----------------------------------------------------------------
        % Compute the quantity of activity (norm of the 3D acceleration)
        quantityAct = sqrt(acc(:,1).^2+acc(:,2).^2+acc(:,3).^2);

        % Compute the mean value of acceleration norm on successive time-windows
        tWindow = 15*f; % 15 s window
        t2      = 1;
        for t = 1:tWindow:size(quantityAct,1)-tWindow
            quantityActAveraged(t2) = mean(quantityAct(t:t+tWindow-1));
            t2                      = t2+1;
        end
        quantityActAveraged(end+1)  = mean(quantityAct(t+tWindow:end));
        quantityActAveragedSum      = sum(quantityActAveraged);
        quantityActAveragedMean     = mean(quantityActAveraged);
        temp                        = sort(quantityActAveraged,'descend');
        quantityActAveragedMax      = mean(temp(1:5));
        quantityActAveragedMin      = min(quantityActAveraged);
        
        % Compute the % of time of activity > a predefined threshold
        timer = 0;
        for t = 1:size(quantityActAveraged,2)
    %         if quantityActAveraged(t) >= quantityActAveragedMin + ...
    %                                      (quantityActAveragedMax - ...
    %                                       quantityActAveragedMin)/2
            if quantityActAveraged(t) >= 10 % arbitrarily defined
                timer = timer+1;
            end
        end
        durationActAveragedMax = timer*100/size(quantityActAveraged,2);
    
        % COMPUTE THE DURATION OF SITTING AND STANDING POSTURES
        % -----------------------------------------------------------------           
        % Compute the mean value of accelerations on successive time-windows
        tWindow = 15*f; % 15 s window
        t2      = 1;
        for t = 1:tWindow:size(acc,1)-tWindow
            accAveraged(t2,1) = mean(acc(t:t+tWindow-1,1));
            accAveraged(t2,2) = mean(acc(t:t+tWindow-1,2));
            accAveraged(t2,3) = mean(acc(t:t+tWindow-1,3));
            t2                = t2+1;
        end
        accAveraged(end+1,1) = mean(acc(t+tWindow:end,1));
        accAveraged(end+1,2) = mean(acc(t+tWindow:end,2));
        accAveraged(end+1,3) = mean(acc(t+tWindow:end,3));
    
        % Compute the duration of each axis
        % The axis with an acceleration close to 9.81 is stored at each frame
        for t = 1:size(accAveraged,1)
            [value(t),axis(t)] = min(abs(abs(accAveraged(t,:))-9.81));
        end
        axisDurationTemp(1) = length(find(axis==1))*15; % s
        axisDurationTemp(2) = length(find(axis==2))*15; % s
        axisDurationTemp(3) = length(find(axis==3))*15; % s
    
        % Find which axes characterise sitting and standing postures
        % - Select manually a sitting segment (axisCalib1)
        % - Set the second most used axis as standing (axisCalib2)
        figure(1); 
        subplot(2,1,1); plot(accAveraged); xlim([0 250]); 
        subplot(2,1,2); plot(axis); ylim([0 5]); xlim([0 250]);
        temp = round(ginput(2));
        close all;
        tCalib = temp(:,1);
        if nanmean(axis(tCalib)) == 1
            axisCalib1             = 1;          % axis related to sitting
            axisDuration(1)        = axisDurationTemp(1);
            temp                   = axis;
            temp(temp==axisCalib1) = NaN;
            axisCalib2             = mode(temp); % axis related to standing
            if mode(temp) == 2
                axisDuration(2)        = axisDurationTemp(2);
                axisDuration(3)        = axisDurationTemp(3);
            else
                axisDuration(2)        = axisDurationTemp(3);
                axisDuration(3)        = axisDurationTemp(2);
            end
        elseif nanmean(axis(tCalib)) == 2
            axisCalib1             = 2;          % axis related to sitting
            axisDuration(1)        = axisDurationTemp(2);
            temp                   = axis;
            temp(temp==axisCalib1) = NaN;
            axisCalib2             = mode(temp); % axis related to standing
            if mode(temp) == 1
                axisDuration(2)        = axisDurationTemp(1);
                axisDuration(3)        = axisDurationTemp(3);
            else
                axisDuration(2)        = axisDurationTemp(3);
                axisDuration(3)        = axisDurationTemp(1);
            end
        elseif nanmean(axis(tCalib)) == 3
            axisCalib1             = 3;          % axis related to sitting
            axisDuration(1)        = axisDurationTemp(3);
            temp                   = axis;
            temp(temp==axisCalib1) = NaN;
            axisCalib2             = mode(temp); % axis related to standing
            if mode(temp) == 2
                axisDuration(2)        = axisDurationTemp(2);
                axisDuration(3)        = axisDurationTemp(1);
            else
                axisDuration(2)        = axisDurationTemp(1);
                axisDuration(3)        = axisDurationTemp(2);
            end
        end
        
        % Count the number of sit-to-stand (nSst) and stand-to-sit (nSts)
        % Correspond to the variation from axisCalib1 <-> axisCalib2
        nSst = 0;
        nSts = 0;
        for t = 1:size(axis,2)-1
            if axis(t) == axisCalib1 && axis(t+1) == axisCalib2
                nSst = nSst+1;
            elseif axis(t) == axisCalib2 && axis(t+1) == axisCalib1
                nSts = nSts+1;
            end
        end

        % STORE SESSION INFORMATION
        % -----------------------------------------------------------------
        Outcomes(icondition).participant = binFiles(icondition).name(8:9);
        Outcomes(icondition).week        = binFiles(icondition).name(11:11);
        Outcomes(icondition).session     = binFiles(icondition).name(13:13);
        Outcomes(icondition).condition   = binFiles(icondition).name(14:14);
        
        % STORE SESSION OUTCOMES /lowerlimb
        % -----------------------------------------------------------------
        Outcomes(icondition).file                              = binFiles(icondition).name;
        Outcomes(icondition).Lowerlimb.sessionDuration         = size(acc,1)/f/60;
        Outcomes(icondition).Lowerlimb.quantityActAveraged     = quantityActAveraged;
        Outcomes(icondition).Lowerlimb.quantityActAveragedSum  = quantityActAveragedSum;
        Outcomes(icondition).Lowerlimb.quantityActAveragedMean = quantityActAveragedMean;
        Outcomes(icondition).Lowerlimb.durationActAveragedMax  = durationActAveragedMax;
        Outcomes(icondition).Lowerlimb.activity1Duration       = axisDuration(1)/60;
        Outcomes(icondition).Lowerlimb.activity2Duration       = axisDuration(2)/60;
        Outcomes(icondition).Lowerlimb.activity3Duration       = axisDuration(3)/60;
                
%         % Create file
%         if ~isfile([subjectID,'.xlsx'])
%             system(['copy ',template,' ',subjectID,'.xlsx']);
%         else
%             disp('Subject file already exists !')
%         end
    
%         % Subject and conditions
%         xlswrite([subjectID,'.xlsx'],cellstr(subjectID),'Rapport','B2');
%         if icondition == 1
%             xlswrite([subjectID,'.xlsx'],cellstr(date),'Rapport','B3');
%         elseif icondition == 2
%             xlswrite([subjectID,'.xlsx'],cellstr(date),'Rapport','B4');
%         end
    
%         % Session duration
%         if icondition == 1
%             xlswrite([subjectID,'.xlsx'],size(acc,1)/f/60,'Rapport','B7');
%         elseif icondition == 2
%             xlswrite([subjectID,'.xlsx'],size(acc,1)/f/60,'Rapport','E7');
%         end
    
%         % Quantity of activity
%         if icondition == 1
%             xlswrite([subjectID,'.xlsx'],quantityActAveraged,'Donnees','B2');
%             xlswrite([subjectID,'.xlsx'],quantityActAveragedSum,'Donnees','B3');
%             xlswrite([subjectID,'.xlsx'],quantityActAveragedMean,'Donnees','B4');
%             xlswrite([subjectID,'.xlsx'],durationActAveragedMax,'Donnees','B5');
%         elseif icondition == 2
%             xlswrite([subjectID,'.xlsx'],quantityActAveraged,'Donnees','B6');
%             xlswrite([subjectID,'.xlsx'],quantityActAveragedSum,'Donnees','B7');
%             xlswrite([subjectID,'.xlsx'],quantityActAveragedMean,'Donnees','B8');
%             xlswrite([subjectID,'.xlsx'],durationActAveragedMax,'Donnees','B9');
%         end
    
%         % Type of activity
%         if icondition == 1
%             if axisCalib1 == 1
%                 xlswrite([subjectID,'.xlsx'],axisDuration(1)/60,'Donnees','B16');
%             elseif axisCalib1 == 2
%                 xlswrite([subjectID,'.xlsx'],axisDuration(2)/60,'Donnees','B16');
%             elseif axisCalib1 == 3
%                 xlswrite([subjectID,'.xlsx'],axisDuration(3)/60,'Donnees','B16');
%             end
%             if axisCalib2 == 1
%                 xlswrite([subjectID,'.xlsx'],axisDuration(1)/60,'Donnees','B15');
%             elseif axisCalib2 == 2
%                 xlswrite([subjectID,'.xlsx'],axisDuration(2)/60,'Donnees','B15');
%             elseif axisCalib2 == 3
%                 xlswrite([subjectID,'.xlsx'],axisDuration(3)/60,'Donnees','B15');
%             end
%             temp                   = axis;
%             temp(temp~=axisCalib1) = NaN;
%             temp(temp==axisCalib1) = 1;
%             xlswrite([subjectID,'.xlsx'],temp,'Donnees','B14');
%             temp                   = axis;
%             temp(temp~=axisCalib2) = NaN;
%             temp(temp==axisCalib2) = 1;
%             xlswrite([subjectID,'.xlsx'],temp,'Donnees','B13');
%             xlswrite([subjectID,'.xlsx'],nSst,'Donnees','B18');
%             xlswrite([subjectID,'.xlsx'],nSts,'Donnees','B19');
%         elseif icondition == 2
%             if axisCalib1 == 1
%                 xlswrite([subjectID,'.xlsx'],axisDuration(1)/60,'Donnees','B23');
%             elseif axisCalib1 == 2
%                 xlswrite([subjectID,'.xlsx'],axisDuration(2)/60,'Donnees','B23');
%             elseif axisCalib1 == 3
%                 xlswrite([subjectID,'.xlsx'],axisDuration(3)/60,'Donnees','B23');
%             end
%             if axisCalib2 == 1
%                 xlswrite([subjectID,'.xlsx'],axisDuration(1)/60,'Donnees','B22');
%             elseif axisCalib2 == 2
%                 xlswrite([subjectID,'.xlsx'],axisDuration(2)/60,'Donnees','B22');
%             elseif axisCalib2 == 3
%                 xlswrite([subjectID,'.xlsx'],axisDuration(3)/60,'Donnees','B22');
%             end
%             temp                   = axis;
%             temp(temp~=axisCalib1) = NaN;
%             temp(temp==axisCalib1) = 1;
%             xlswrite([subjectID,'.xlsx'],temp,'Donnees','B21');
%             temp                   = axis;
%             temp(temp~=axisCalib2) = NaN;
%             temp(temp==axisCalib2) = 1;
%             xlswrite([subjectID,'.xlsx'],temp,'Donnees','B20');
%             xlswrite([subjectID,'.xlsx'],nSst,'Donnees','B25');
%             xlswrite([subjectID,'.xlsx'],nSts,'Donnees','B26');
%         end
        
    % ---------------------------------------------------------------------
    % PARAMETERS RELATED TO UPPER LIMB ACTIVITY
    % ---------------------------------------------------------------------        
    elseif strfind(binFiles(icondition).name,'avtbras')
        
        % COMPUTE THE QUANTITY OF ACTIVITY
        % -----------------------------------------------------------------
        % Compute the quantity of activity (norm of the 3D acceleration)
        quantityAct = sqrt(acc(:,1).^2+acc(:,2).^2+acc(:,3).^2);

        % Remove spikes = find peaks > 10*std and set 50 frames before to after
        % the signal at the mean signal value
        temp = (find(abs(quantityAct-mean(quantityAct))>10*std(quantityAct)));
        quantityActFilt = quantityAct;
        for i = 1:length(temp)
            for j = temp(i)-50:temp(i)+50
                quantityActFilt(j) = mean(quantityAct);
            end
        end
%         figure(); hold on;
%         plot(quantityAct,'blue');
%         plot(quantityActFilt,'red');

        % Compute the mean value of acceleration norm on successive time-windows
        tWindow = 15*f; % 15 s window
        t2      = 1;
        for t = 1:tWindow:size(quantityActFilt,1)-tWindow
            quantityActAveraged(t2) = mean(quantityActFilt(t:t+tWindow-1));
            t2                      = t2+1;
        end
        quantityActAveraged(end+1)  = mean(quantityActFilt(t+tWindow:end));
        quantityActAveragedSum      = sum(quantityActAveraged);
        quantityActAveragedMean     = mean(quantityActAveraged);
        temp                        = sort(quantityActAveraged,'descend');
        quantityActAveragedMax      = mean(temp(1:5));
        quantityActAveragedMin      = min(quantityActAveraged);
        
        % Compute the % of time of activity > a predefined threshold
        timer = 0;
        for t = 1:size(quantityActAveraged,2)
    %         if quantityActAveraged(t) >= quantityActAveragedMin + ...
    %                                      (quantityActAveragedMax - ...
    %                                       quantityActAveragedMin)/2
            if quantityActAveraged(t) >= 10 % arbitrarily defined
                timer = timer+1;
            end
        end
        durationActAveragedMax = timer*100/size(quantityActAveraged,2);
    
        % ASSESS THE ARM VERTICALITY
        % -----------------------------------------------------------------            
        % Compute the mean value of accelerations on successive time-windows
        tWindow = 15*f; % 15 s window
        t2      = 1;
        for t = 1:tWindow:size(acc,1)-tWindow
            accAveraged(t2,1) = mean(acc(t:t+tWindow-1,1));
            accAveraged(t2,2) = mean(acc(t:t+tWindow-1,2));
            accAveraged(t2,3) = mean(acc(t:t+tWindow-1,3));
            t2                = t2+1;
        end
        accAveraged(end+1,1) = mean(acc(t+tWindow:end,1));
        accAveraged(end+1,2) = mean(acc(t+tWindow:end,2));
        accAveraged(end+1,3) = mean(acc(t+tWindow:end,3));
    
        % Find the longitudinal axis of the arm, assumed close to vertical
        % Store its acceleration
        for t = 1:size(accAveraged,1)
            [value(t),axis(t)] = min(abs(abs(accAveraged(t,:))-9.81));
        end
        accArm = accAveraged(:,mode(axis));
        
        % Compute the % of time of activity were main axis acceleration 
        % have less than 1 m.s-1 difference with gravity acceleration
        timer = 0;
        for i = 1:size(value,2)
            if axis(i) == mode(axis) % if the axis being mainly vertical is detected
                if value(i) < 1 % if less than 1 m.s-1 difference with gravity acceleration
                    timer = timer+1;
                end
            end
        end
        durationActArmVertical = timer*100/size(quantityActAveraged,2);

        % STORE SESSION INFORMATION
        % -----------------------------------------------------------------
        Outcomes(icondition).participant = binFiles(icondition).name(9:10);
        Outcomes(icondition).week        = binFiles(icondition).name(12:12);
        Outcomes(icondition).session     = binFiles(icondition).name(14:14);
        Outcomes(icondition).condition   = binFiles(icondition).name(15:15);
        
        % STORE SESSION OUTCOMES /upperlimb
        % -----------------------------------------------------------------
        Outcomes(icondition).file                              = binFiles(icondition).name;
        Outcomes(icondition).Upperlimb.sessionDuration         = size(acc,1)/f/60;
        Outcomes(icondition).Upperlimb.quantityActAveraged     = quantityActAveraged;
        Outcomes(icondition).Upperlimb.quantityActAveragedSum  = quantityActAveragedSum;
        Outcomes(icondition).Upperlimb.quantityActAveragedMean = quantityActAveragedMean;
        Outcomes(icondition).Upperlimb.durationActAveragedMax  = durationActAveragedMax;
        Outcomes(icondition).Upperlimb.durationActArmVertical  = durationActArmVertical;

%         % Create file
%         if ~isfile([subjectID,'.xlsx'])
%             system(['copy ',template,' ',subjectID,'.xlsx']);
%         else
%             disp('Subject file already exists !')
%         end
    
%         % Quantity of activity
%         if icondition == 3
%             xlswrite([subjectID,'.xlsx'],quantityActAveraged,'Donnees','B30');
%             xlswrite([subjectID,'.xlsx'],quantityActAveragedSum,'Donnees','B31');
%             xlswrite([subjectID,'.xlsx'],quantityActAveragedMean,'Donnees','B32');
%             xlswrite([subjectID,'.xlsx'],durationActAveragedMax,'Donnees','B33');
%         elseif icondition == 4
%             xlswrite([subjectID,'.xlsx'],quantityActAveraged,'Donnees','B34');
%             xlswrite([subjectID,'.xlsx'],quantityActAveragedSum,'Donnees','B35');
%             xlswrite([subjectID,'.xlsx'],quantityActAveragedMean,'Donnees','B36');
%             xlswrite([subjectID,'.xlsx'],durationActAveragedMax,'Donnees','B37');
%         end
        
%         % Type of posture
%         if icondition == 3
%             xlswrite([subjectID,'.xlsx'],durationActArmVertical,'Donnees','B41');
%         elseif icondition == 4
%             xlswrite([subjectID,'.xlsx'],durationActArmVertical,'Donnees','B42');
%         end
        
    end
    
end